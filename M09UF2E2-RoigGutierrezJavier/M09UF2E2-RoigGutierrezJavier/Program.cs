﻿using System;
using System.IO;
using System.Threading;

namespace M09UF2E2_RoigGutierrezJavier
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Exercicis Threads Parte II");
            Console.WriteLine("1. Exercici 1");
            Console.WriteLine("2. Exercici 2");
            Console.WriteLine("Escogeix l'exercici a executar: ");
            int resposta = Convert.ToInt32(Console.ReadLine());
            switch (resposta)
            {
                case 1:
                    Exercici1();
                    break;
                case 2:
                    Exercici2();
                    break;
                default:
                    Console.WriteLine("Error.");
                    break;
            }
        }

        static void Exercici1()
        {
            //bool threading = true;
            Thread thread1 = new Thread(() =>
            {
                Write(ReadFileLines(1));
            });
            Thread thread2 = new Thread(() =>
            {
                thread1.Join();
                Write(ReadFileLines(2));
            });
            Thread thread3 = new Thread(() =>
            {
                thread2.Join();
                Write(ReadFileLines(3));
            });
            thread1.Start();
            thread2.Start();
            thread3.Start();
            //threading = false;
        }

        static void Exercici2()
        {
            Nevera neveraCantantes = new Nevera();
            neveraCantantes.OmplirNevera("Anitta");

            Thread thread1 = new Thread(() =>
            {
                neveraCantantes.BeureCervesa("Bad Bunny");
            });
            Thread thread2 = new Thread(() =>
            {
                thread1.Join();
                neveraCantantes.BeureCervesa("Lil Nas X");
            });
            Thread thread3 = new Thread(() =>
            {
                thread2.Join();
                neveraCantantes.BeureCervesa("Manuel Turizo");
            });
            thread1.Start();
            thread2.Start();
            thread3.Start();
        }

        static string [] ReadFileLines(int i)
        {
            string filePath = @"..\..\..\Frases.txt";
            using (StreamReader readFile = new StreamReader(filePath))
            {
                int lines = 0;
                string frase;
                string[] words = new string[] { };
                while (readFile.ReadLine() != null)
                {
                    lines++;
                    if (lines == i)
                    {
                        frase = readFile.ReadLine();
                        words = frase.Split(' ');
                        return words;
                    }
                    
                }
                return words;
            }
        }

        static void Write(string [] words)
        {
            for (int i = 0; i < words.Length; i++)
            {
                Thread.Sleep(1000);
                Console.Write(" " + words[i]);
            }
            Console.WriteLine();
        }
    }

    public class Nevera
    {
        protected int cerveses;
        public Nevera()
        {
            cerveses = 0;
        }

        public void OmplirNevera(string persona)
        {
            Thread.Sleep(1000);
            Random rnd = new Random();
            int num = rnd.Next(0, 7);
            int cerevecesPrevias;
            if (cerveses == 9)
            {
                Console.WriteLine(persona + " no ha podido llenar la navera porque ya esta llena.");
            }
            else if (cerveses + num > 9)
            {
                cerevecesPrevias = cerveses;
                cerveses = 9;
                Console.WriteLine(persona + " ha llenado la nevera pero se le han quedado " + (num - (9 - cerevecesPrevias)) + " cervezas fuera.");
            }
            else
            {
                cerveses += num;
                Console.WriteLine(persona + " ha llenado la nevera con " + num + " cervezas.");
            }
        }

        public void BeureCervesa(string persona)
        {
            Thread.Sleep(1000);
            Random rnd = new Random();
            int num = rnd.Next(0, 7);
            int cerevecesPrevias;
            if (cerveses == 0)
            {
                Console.WriteLine(persona + " no ha podido beber ninguna cerbeza porque no quedan.");
            }
            else if (cerveses - num < 1)
            {
                cerevecesPrevias = cerveses;
                cerveses = 0;
                Console.WriteLine(persona + " se ha bebido " + (0 + cerevecesPrevias) + " cervezas pero queria beberse " + num);
            }
            else
            {
                cerveses -= num;
                Console.WriteLine(persona + " se ha bebido " + num + " cervezas.");
            }
        }
    }
}
